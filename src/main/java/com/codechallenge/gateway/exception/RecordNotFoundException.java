package com.codechallenge.gateway.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * A class that captures exception thrown when an entity is missing in the datastore.
 *
 * @author  Elvis
 * @version 1.0, 9/3/18
 * @see     java.lang.Exception
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Oops! Could not find record entity with id")
public class RecordNotFoundException extends Exception
{
    static final long serialVersionUID = -3387516993334229948L;

    /**
	* This constructor allows you to override the default message on missing record exception thrown
	*
	* @param <b>message</b> this is the exception message
	* @exception RecordNotFoundException
	*/
    public RecordNotFoundException(String message)
    {
        super(message);
    }

}
