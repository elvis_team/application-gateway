package com.codechallenge.gateway.service;


import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.codechallenge.gateway.model.AdClick;
import com.codechallenge.gateway.model.AdDelivery;
import com.codechallenge.gateway.model.AdInstall;
import com.codechallenge.gateway.model.ResponseWrapper;

/**
 * This class defines the simple restclient to post to the ingestion service
 * Load balancing can also be implemented here.
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
//@FeignClient(name="apis-gateway-service") 
@FeignClient(name="ingestion-service", url="http://myingestionservice:8100")
@RibbonClient(name="ingestion-service") 
public interface IngestionServiceProxy {
	
	@PostMapping(value="/ingestion/delivery")
	public ResponseWrapper sendDeliveryEvent(@RequestBody AdDelivery adDelivery);
	
	@PostMapping(value="/ingestion/click")
	public ResponseWrapper sendClickEvent(@RequestBody AdClick adClick);
	
	@PostMapping(value="/ingestion/install")
	public ResponseWrapper sendInstallEvent(@RequestBody AdInstall adInstall);

}
