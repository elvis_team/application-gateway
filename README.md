##	Event Tracker

This is a simple AdTech web service that tracks the ads that are being delivered through their lifecycle and generates some simple statistics.

In order to have a scalable and highly available system, I have deployed the major components as microservices. This can easily be deployed on several instances if scalability is required on any of the services and a load balancer setup on them ensures that traffic are evenly distributed among the instances. This I implemented using Spring Boot and Spring Cloud.

A messaging/Queuing system - Kafka was also implemented to cater for high volumes of requests(Ingestion) and ensure the requests can be processed in parallel through the concept of consumer-groups that Kafka provides.

To ensure high availability and speedy query for our data, I implemented a NOSQL (Cassandra) database as my datastore.

This system currently comprises the following (docker compliant) services:

- Application Gateway Service: This is the entry point for the application (**Port: 8400**).

- Ingestion Service: This provides the input channel to our system (**Port: 8100**).

- Parser Service: This parses all inputs in our queuing system and store them on our distributed datastore (**Port: 8200**).

- Cassandra Service: Distributed, highly available database (**Port: 9042**).

- Kafkabroker: Queuing/Messaging system (**Port: 9092**), Dashboard (**Port: 3030**).

- Config Service: Provides a central property file for our microservices (**Port: 8888**).

- Statisitics Service: This exposes all statistics endpoints (**Port: 8300**).

- Discovery Service: This provides a dashboard to monitor and coordinate our *(5+)* microservices (**Port: 8761**). Please verify you can access this service *(e.g http://localhost:8761)* and that the 5+ services are up before testing the endpoints on port *8400*.

![Data Flow Diagram](data-pipeline.png)

######Application Requirements:
- Internet Connection
- Java 8
- Maven 3+
- Docker and `docker-compose` (This is required to run on docker containers)

######Deploying and Running application on docker (EasyWay):
> (Optional - images available at hub.docker.com) To build each service before running.
```
~$ mvn clean package docker:build
```
> Running the application.
```
~$ docker-compose up -d
```
Ensure to run the above command from the directory with the docker-compose.yml file. Kindly wait between 5 - 10 mins
to have your containers(5+) up and running before hitting the system. See `http://localhost:8761` to verify

######Deploying and Running application (HardWay):
> This should be done for all services
```
~$ mvn clean package spring-boot:run
```
Ones the services are up, the application `gateway service` can be accessed on port 8400.

Always visit http://~:8761 to see if all the 6 services are up before sending requests. This can take between 5 - 10 minutes to start.
	
				
##   Cheers! 

