package com.codechallenge.gateway.service;


import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.codechallenge.gateway.model.ResponseWrapper;

/**
 * This class defines the simple restclient to GET from the statistics service
 * Load balancing can also be implemented here.
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
//@FeignClient(name="apis-gateway-service") 
@FeignClient(name="statistics-service", url="http://mystatisticsservice:8300") 
@RibbonClient(name="statistics-service") 
public interface StatisticsServiceProxy {
	
	@GetMapping("/statistics/delivery/{id}")
	public ResponseWrapper checkDeliveryId(@PathVariable("id") String id);
	
	@GetMapping("/statistics/click/{id}")
	public ResponseWrapper checkClickId(@PathVariable("id") String id);
	
	@GetMapping("/statistics/summary")
	public ResponseWrapper getSummary(@RequestParam("start") String start, @RequestParam("end") String end);
	
	@GetMapping("/statistics/summary")
	public ResponseWrapper getSummaryByGroup(@RequestParam("start") String start, @RequestParam("end") String end, @RequestParam("group_by") String ...group);
	
}
