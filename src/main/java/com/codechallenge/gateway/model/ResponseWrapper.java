package com.codechallenge.gateway.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseWrapper {

	// Define Success placeholders
	private String key = null;
	private Map<String, Date> interval = null;
	private Map<String, Integer> stats = null;
	private List<Map<String,Map<String,String>>> data = null;
	
	// Define Exception placeholders
	private String message = null;
	private String reason = null;
	private String path = null;
	private String error = null;
	private Integer status = null;
	private String timestamp = null;
	
}
