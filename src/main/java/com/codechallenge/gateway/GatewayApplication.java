package com.codechallenge.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * This class defines the entry point to our system. All request should be directed to this service
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@SpringBootApplication
@EnableFeignClients("com.codechallenge.gateway.service")
@EnableDiscoveryClient
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
	
}
