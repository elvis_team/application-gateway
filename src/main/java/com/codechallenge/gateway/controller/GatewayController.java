package com.codechallenge.gateway.controller;

import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codechallenge.gateway.model.AdClick;
import com.codechallenge.gateway.model.AdDelivery;
import com.codechallenge.gateway.model.AdInstall;
import com.codechallenge.gateway.model.ResponseWrapper;
import com.codechallenge.gateway.service.IngestionServiceProxy;
import com.codechallenge.gateway.service.StatisticsServiceProxy;

import lombok.extern.slf4j.Slf4j;


/**
 * This class defines the ingestion and statistics endpoints
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@RestController
@RequestMapping("/ads")
@Slf4j
public class GatewayController {

	@Autowired
	private IngestionServiceProxy ingestionProxy;
	
	@Autowired
	private StatisticsServiceProxy statisticsProxy;
	
	/**
	* This method POST new delivery or pageload request to our queue via the ingestion service
	*
	* @param <b>advertisementId (Integer)</b> this is the ad id
	* @param <b>deliveryId (String)</b> UUID format string representing the adload id
	* @param <b>time (String)</b> Date format string representing the event time
	* @param <b>browser (String)</b> Browser where ad was loaded
	* @param <b>os (String)</b> the client device operating system used.
	* @param <b>site (String)</b> the source site uri.
	* @return <b>responseEntity (ResponseEntity<ResponseWrapper>)</b> holding the state of the request - OK or ERROR
	* @throws Exception
	*/
	@PostMapping(value="/delivery", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendDeliveryEvent(@RequestBody AdDelivery adDelivery) {
		log.info("Gateway: Posting delivery content - {}", adDelivery);
		ResponseWrapper response = ingestionProxy.sendDeliveryEvent(adDelivery);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	/**
	* This method POST new click event to our queue via the ingestion service
	*
	* @param <b>deliveryId (String)</b> UUID format string representing the adload id
	* @param <b>clickId (String)</b> UUID format string representing the ad click id
	* @param <b>time (String)</b> Date format string representing the event time
	* @return <b>responseEntity (ResponseEntity<ResponseWrapper>)</b> holding the state of the request - OK or ERROR
	* @throws FileNotFoundException, Exception
	*/
	@PostMapping(value="/click", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendClickEvent(@RequestBody AdClick adClick) {
		
		log.info("Gateway: Checking delivery id - {}", adClick.getDeliveryId());
		ResponseWrapper response = statisticsProxy.checkDeliveryId(adClick.getDeliveryId());
		log.info("Delivery Id response {}", response);
		if(response.getKey().equals("404")) {
			response.setStatus(404); response.setKey(null);
			response.setMessage("Sorry, we never received a delivery " + adClick.getDeliveryId());
			return new ResponseEntity<ResponseWrapper>(response, HttpStatus.NOT_FOUND);
		}
		log.info("Gateway: Posting click content - {}", adClick);
		response = ingestionProxy.sendClickEvent(adClick);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	/**
	* This method POST new install event to our queue via the ingestion service
	*
	* @param <b>installId (String)</b> UUID format string representing the ad install id
	* @param <b>clickId (String)</b> UUID format string representing the ad click id
	* @param <b>time (String)</b> Date format string representing the event time
	* @return <b>responseEntity (ResponseEntity<ResponseWrapper>)</b> holding the state of the request - OK or ERROR
	* @throws FileNotFoundException, Exception
	*/
	@PostMapping(value="/install", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> sendInstallEvent(@RequestBody AdInstall adInstall) {
		log.info("Gateway: Checking click id - {}", adInstall.getClickId());
		ResponseWrapper response = statisticsProxy.checkClickId(adInstall.getClickId());
		log.info("Click Id response {}", response);
		if(response.getKey().equals("404")) {
			response.setStatus(404); response.setKey(null);
			response.setMessage("Sorry, we never received a click " + adInstall.getClickId());
			return new ResponseEntity<ResponseWrapper>(response, HttpStatus.NOT_FOUND);
		}
		log.info("Gateway: Posting install content - {}", adInstall);
		response = ingestionProxy.sendInstallEvent(adInstall);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	/**
	* This method GET views, clicks, and installs via the statistics service
	*
	* @param <b>start (String)</b> String date format specify the start of period to query 
	* @param <b>end (String)</b> String date format specify the end of period to query 
	* @return <b>responseEntity (ResponseEntity<ResponseWrapper>)</b> holding the state of the request - OK or ERROR
	* @throws ParserException, Exception
	*/
	@GetMapping(value="/statistics", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> summary(@RequestParam("start") String start, @RequestParam("end") String end) throws ParseException {
		log.info("Gateway: Getting summary between {} - {}", start, end);
		ResponseWrapper response = statisticsProxy.getSummary(start, end);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	/**
	* This method GET views, clicks, and installs, group_by, via the statistics service
	*
	* @param <b>start (String)</b> String date format specify the start of period to query 
	* @param <b>end (String)</b> String date format specify the end of period to query 
	* @param <b>group_by (Array of Strings)</b> specifies the category to group your queries by. 
	* @return <b>responseEntity (ResponseEntity<ResponseWrapper>)</b> holding the state of the request - OK or ERROR
	* @throws ParseException, Exception
	*/
	@GetMapping(value="/statistics",  params = {"group_by"}, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> summaryByBrowserAndOS(@RequestParam("start") String start, @RequestParam("end") String end, @RequestParam("group_by") String ...group_by) throws ParseException {
		log.info("Gateway: Getting summary between {} - {} group_by {}", start, end, group_by);
		ResponseWrapper response = statisticsProxy.getSummaryByGroup(start, end, group_by);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
}
