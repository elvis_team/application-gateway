package com.codechallenge.gateway.exception;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A class that captures application wide exception.
 *
 * @author  Elvis
 * @version 1.0, 9/03/18
 * @see     org.springframework.web.bind.annotation.ControllerAdvice
 */
@ControllerAdvice
public class GlobalExceptionsHandler {
	
	/**
	 * Handle application wide RecordNotFoundException exception.
	 * @return ResponseEntity<Map<K, V>> of exception detail. K = 'message' | 'reason' | 'path' | 'error' | 'status' | 'timestamp'
	 * @exception  RecordNotFoundException
	 */
	@ExceptionHandler(RecordNotFoundException.class)
	public  ResponseEntity<Error> handleMediaException(RecordNotFoundException se, HttpServletRequest request) {
		
		Error error = new Error(se.getMessage(), String.valueOf(se.getCause()), request.getRequestURI(), "Record Not Found Exception", HttpStatus.NOT_FOUND.value(), (new Date()).toInstant().toString());
		
		return new ResponseEntity<Error>(error, HttpStatus.NOT_FOUND);
		
	}
	
	/**
	 * Handle application wide Generic/Undefined exception.
	 *
	 * @return ResponseEntity<Map<K, V>> of exception detail. K = 'message' | 'reason' | 'path' | 'error' | 'status' | 'timestamp'
	 * @exception  Exception
	 */
	@ExceptionHandler(Exception.class)
	public @ResponseBody Object handleGeneralException(HttpServletRequest request, Exception e) throws Exception {
		e.printStackTrace();
		Error error = new Error(e.getMessage(), String.valueOf(e.getCause()), request.getRequestURI(), "Oops! something went wrong", HttpStatus.INTERNAL_SERVER_ERROR.value(), (new Date()).toInstant().toString());
		
		return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
