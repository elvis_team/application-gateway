package com.codechallenge.gateway.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Error {
	
	private String message;
	private String reason;
	private String path;
	private String error;
	private Integer status;
	private String timestamp;
	
}
